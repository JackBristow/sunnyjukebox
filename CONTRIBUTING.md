# Contributing to Sunny Jukebox

Thank you for considering contributing to Sunny Jukebox! We welcome contributions from the community to help improve and grow this project. Whether you're a developer, designer, tester, or just someone with a great idea, your input is valuable to us.

## Getting Started

To contribute to Sunny Jukebox, follow these steps:

1. **Fork the Repository**: Click the "Fork" button at the top-right corner of this repository to create your own copy.

2. **Clone the Repository**: Clone your forked repository to your local machine using Git:
   ```
   git clone https://gitlab.com/JackBristow/sunnyjukebox
   ```

3. **Install Dependencies**: Navigate to the project directory and install the necessary dependencies for both the frontend and backend:
   ```
   cd sunnyjukebox
   npm install      # Install frontend dependencies for Svelte
   pip install -r requirements.txt   # Install backend dependencies for Python/FastAPI
   ```

4. **Run the Development Server**: Start the frontend and backend development servers:
   ```
   # Frontend (Svelte)
   npm run dev

   # Backend (Python/FastAPI)
   uvicorn app.main:app --reload
   ```

5. **Open the Application**: Visit `http://localhost:3000` in your web browser to view the running application.

## Contribution Guidelines

Before making contributions, please ensure that you follow these guidelines:

- **Code Style**: Follow consistent coding style and conventions. For frontend, adhere to the Svelte and JavaScript/HTML/CSS best practices. For backend, follow Python and FastAPI conventions.
- **Testing**: Write tests for new features and bug fixes. Ensure that all existing tests pass.
- **Documentation**: Update relevant documentation, including README files, whenever necessary.
- **Commit Messages**: Write clear and descriptive commit messages, following the conventional commit format is encouraged.
- **Pull Requests**: Submit pull requests against the `main` branch. Provide a detailed description of the changes and ensure that your branch is up-to-date with the main branch before creating a pull request.

## Communication

If you have any questions, feedback, or need assistance, don't hesitate to reach out:

- **Issue Tracker**: Use GitLab Issues to report bugs, suggest new features, or discuss improvements.
- **Discussions**: Participate in GitLab Discussions for general discussions, questions, and announcements.
- **Discord**: You can also contact us via Discord at [Sunny Planet](https://discord.gg/cGRBnGXNAG).

## Code of Conduct

Please note that this project is governed by the [Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [project maintainers](CONTRIBUTORS.md).

## License

By contributing to Sunny Jukebox, you agree that your contributions will be licensed under the [MIT License](LICENSE).

We appreciate your contributions to make Sunny Jukebox a better platform for music lovers everywhere!
