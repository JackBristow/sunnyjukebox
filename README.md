# Sunny Jukebox

Welcome to Sunny Jukebox, the open-source repo for [sunnyjukebox.com](https://sunnyjukebox.com); allowing users to create a collaborative playlist and stream tracks in real-time. With Sunny Jukebox, users can sign up, submit their favorite tracks or beats, and enjoy listening to a dynamic playlist together with others.

## Features

- **User Authentication**: Users can sign up and log in securely to access the jukebox functionality.
- **Playlist Management**: Users can add tracks or beats to the playlist.
- **Real-time Streaming**: The playlist is streamed in real-time on a video stream, allowing users to enjoy music together simultaneously.
- **Collaborative Experience**: Multiple users can contribute to the playlist, creating a collaborative and diverse listening experience.

## Installation

To install and run Sunny Jukebox locally, follow these steps:

1. Clone the repository: 
   ```
   git clone https://gitlab.com/JackBristow/sunnyjukebox
   ```

2. Navigate to the project directory:
   ```
   cd sunnyjukebox
   ```

3. Install dependencies:
   ```
   npm install
   ```

4. Set up environment variables by creating a `.env` file in the root directory and add the following variables:
   ```
   EVENTUAL ENVIRONMENT VARIABLES
   ```

5. Start the server:
   ```
   npm start
   ```

6. Open your browser and visit `http://localhost:3000` to access Sunny Jukebox.

## Usage

1. Sign up for an account if you're a new user, or log in if you already have an account.
2. Once logged in, you'll be redirected to the main page where you can see the current playlist.
3. To add a track or beat to the playlist, click on the "Add Track" button and fill in the required information.
4. You can manage the playlist by adding new ones.
5. Enjoy listening to the playlist together with other users in real-time!

## Contributing

We welcome contributions from the community to make Sunny Jukebox even better. To contribute, follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix: 
   ```
   git checkout -b feature/your-feature-name
   ```
3. Make your changes and commit them with descriptive commit messages.
4. Push your changes to your fork:
   ```
   git push origin feature/your-feature-name
   ```
5. Create a pull request to merge your changes into the main repository.

## License

Sunny Jukebox is licensed under the [MIT License](LICENSE).

## Contact

If you have any questions, suggestions, or feedback, feel free to reach out to us at [the planet sunny Discord](https://discord.gg/cGRBnGXNAG).

Happy listening! 🎶
